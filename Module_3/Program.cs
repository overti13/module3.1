﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            int num;
            if (source == "0") return 0;
            Int32.TryParse(source, out num);
            if (num == 0 || num < 0)
            {
                source = Console.ReadLine();
                return ParseAndValidateIntegerNumber(source);
            }
            return num;
        }

        public int Multiplication(int num1, int num2)
        {
            int chislo = num1;
            if (num2 > 0) for (int i = 1; i < num2; i++) chislo += num1;
            else for (int i = -1; i < -num2; i++) chislo -= num1;
            return chislo;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            result = ParseAndValidateIntegerNumber(input);
            return true;
        }

        public int ParseAndValidateIntegerNumber(string source)
        {
            int num;
            if (source == "0") return 0;
            Int32.TryParse(source, out num);
            if (num == 0 || num < 0)
            {
                source = Console.ReadLine();
                return ParseAndValidateIntegerNumber(source);
            }
            return num;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> natural = new List<int>();
            int k = 0;
            for (int i = 0; i < naturalNumber; i++)
            {
                natural.Insert(i, k);
                k += 2;
            }
            return natural;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            result = ParseAndValidateIntegerNumber(input);
            return true;
        }

        public int ParseAndValidateIntegerNumber(string source)
        {
            int num;
            if (source == "0") return 0;
            Int32.TryParse(source, out num);
            if (num == 0 || num < 0)
            {
                source = Console.ReadLine();
                return ParseAndValidateIntegerNumber(source);
            }
            return num;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string chislo = source.ToString();
            char cifra = Convert.ToChar(digitToRemove);
            for (int i = 0; i < chislo.Length; i++)
            {
                if (chislo[i] == cifra)
                    chislo = chislo.Remove(i, 1);
            }
            return chislo;
        }
    }
}
